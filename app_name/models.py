from django.db import models
from django import forms
from django.contrib.auth.models import User


class Directory(models.Model):
    name = models.CharField(max_length = 50, unique=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    parent_directory = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    flag_available = models.BooleanField(default=True)
    creation_date = models.DateField(auto_now_add=True)
    description = models.CharField(max_length = 100, null=True, blank=True)

    def __str__(self):
        return self.name


class File(models.Model):
    name = models.CharField(max_length = 50, unique=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    parent_directory = models.ForeignKey(Directory, on_delete=models.CASCADE, null=True, blank=True)
    flag_available = models.BooleanField(default=True)
    creation_date = models.DateField(auto_now_add=True)
    description = models.CharField(max_length = 100, null=True, blank=True)
    chosen_prover = models.CharField(max_length = 10, default="alt-ergo")
    chosen_flags = models.CharField(max_length = 50, default=" ")

    class Meta:
        unique_together = (('name', 'owner'))

    def __str__(self):
        return self.name


class StatusData(models.Model):
    data = models.CharField(max_length = 999, default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    creation_date = models.DateField(auto_now_add=True)
    flag_valid = models.BooleanField(default=True)

    def __str__(self):
        return self.data


class FileSection(models.Model):
    filename = models.CharField(max_length=50, null=True, blank=True)
    description = models.CharField(max_length = 999, null=True, blank=True)
    creation_date = models.DateField(auto_now_add=True)
    section_category = models.CharField(default='Invariant', max_length=10)
    status = models.CharField(default='Unchecked', max_length=10)
    status_color = models.CharField(default='black', max_length=20)
    status_data = models.ForeignKey(StatusData, on_delete=models.CASCADE)
    flag_valid = models.BooleanField(default=True)

    def __str__(self):
        return self.description
