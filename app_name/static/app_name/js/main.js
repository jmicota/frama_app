function showTab(number) {
    for (i = 1; i < 4; i++) {
        if (i == number) {
            document.getElementById("tab" + i.toString()).classList.add("visible");
            document.getElementById("tb" + i.toString()).classList.add("highlight");
        }
        else {
            document.getElementById("tab" + i.toString()).classList.remove("visible");
            document.getElementById("tb" + i.toString()).classList.remove("highlight");
        }
    }
}

function setLight() {
    var r = document.querySelector(':root');
    r.style.setProperty('--white-base', 'var(--my-white-base)');
    r.style.setProperty('--text-color', 'black');
    r.style.setProperty('--base-background', 'var(--lightmode-darkblue)');
    r.style.setProperty('--light-extra', 'var(--lightmode-lightblue)');
}

function setDark() {
    var r = document.querySelector(':root');
    r.style.setProperty('--white-base', 'var(--my-black-base)');
    r.style.setProperty('--text-color', 'white');
    r.style.setProperty('--base-background', 'var(--darkmode-darkblue)');
    r.style.setProperty('--light-extra', 'var(--darkmode-lightblue)');
}

function get_file(val) {
    event.preventDefault();
    var data = {"file": val}
    $.ajax({
        type: 'GET',
        data: data,
        url: `/choose_file`,
        dataType: 'text',
        success: function(data) {
            var response = JSON.parse(data);
            document.getElementById('textarea2').innerHTML = response.file[3];
            document.getElementById('right-iframe-dsc').innerHTML = response.file[1] + " output:";
            document.getElementById('bottom-textarea').innerHTML = '';
            var editor = $('#textarea1').data('CodeMirrorInstance');
            editor.getDoc().setValue(response.file[2]);
        }
    })
}

function run_prover() {
    event.preventDefault();
    var editor = $('#textarea1').data('CodeMirrorInstance');
    var code = editor.getValue();
    var data = {"code": code}
    $.ajax({
        type: 'GET',
        data: data,
        url: `/run_prover`,
        dataType: 'text',
        success: function(data) {
            var response = JSON.parse(data);
            document.getElementById('bottom-textarea').innerHTML = response.output[0];
            document.getElementById('textarea2').innerHTML = response.output[1];
        }
    })
}
