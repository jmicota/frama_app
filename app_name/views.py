import re
import os
from django.utils.timezone import datetime, timedelta
from django.shortcuts import render
from django.http import HttpResponse
from .models import Directory, File, FileSection, StatusData
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from os.path import join
from django.conf import settings
from django.core.serializers import serialize
from django.forms.models import model_to_dict
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
from django.contrib import messages


def register(request):
    if request.method == 'POST':
        f = UserCreationForm(request.POST)
        if f.is_valid():
            f.save()
            messages.success(request, 'Account created successfully')
            return redirect('login')
    else:
        f = UserCreationForm()
    return render(request, 'app_name/register.html', {'form': f})


@login_required
def getHtml(request):
    default_files = File.objects.all().filter(owner=request.user)
    default_files = default_files.filter(flag_available=True)
    n = default_files.count()
    if n == 0:
        return display_file(request, -1)

    default_file = default_files.first()
    default_file_pk = default_file.pk
    return display_file(request, default_file_pk)


# build path for file containing frama output
def result_path(path):
    return path[:-2] + '-result.c'

# helper methods for parsing frama output
def section_name(line):
    if (line.startswith('Goal Post-condition')):
        return 'Post-condition'
    if (line.startswith('Goal Pre-condition')):
        return 'Pre-condition'
    if (line.startswith('Goal Assertion')):
        return 'Assertion'
    if (line.startswith('Goal Preservation of Invariant') or 
        line.startswith('Goal Establishment of Invariant')):
        return 'Invariant'
    return "";

def status(line):
    chunks = line.split(' ')
    return chunks[3]

def status_color(status):
    if status == 'Invalid':
        return 'Crimson'
    elif status == 'Valid':
        return 'green'
    elif status == 'Unknown':
        return 'BlueViolet'
    else:
        return 'DimGrey'

def parse_result(content, filename):
    section_found = False
    for line in content:
        if not section_found: # found section
            section = section_name(line)
            if section != "":
                section_found = True
                fs = FileSection()
                fs.filename = filename
                fs.section_category = section
                stdt = StatusData()
                stdt.save()
                fs.status_data = stdt
                fs.description = line + '\n'
        elif line.startswith('Prover'): # found end of section
            section_found = False
            fs.description = fs.description + line + '\n'
            fs.status = status(line)
            fs.status_color = status_color(fs.status)
            fs.save()
        elif section_found: # extending section data
            fs.description = fs.description + line + '\n'


@login_required
def add_file(request):
    existing_dirs = Directory.objects.all().filter(flag_available=True)
    context = {'dirs' : existing_dirs.filter(owner=request.user)}
    if request.method == 'POST' and request.POST.get('fname') and \
        request.POST.get('fpdir') and \
        request.POST.get('fcont') and \
        request.POST.get('fdesc'):
        f=File()
        f.name= request.POST.get('fname')
        f.owner= request.user
        if request.POST.get('fpdir') != "/":
            f.parent_directory= Directory.objects.get(name=request.POST.get('fpdir'))
        f.description = request.POST.get('fdesc')
        content = request.POST.get('fcont')
        f.save()

        path = join(settings.MEDIA_ROOT, 'app_name', 'user_data', f.name)
        file = open(path, "w")
        file.write(content)
        file.close()

        path_result = result_path(path)
        command = 'frama-c -wp -wp-print ' + path
        frama_output = os.popen(command).read()
        parse_result(frama_output.split('\n'), request.POST.get('fname'))

        file_result = open(path_result, "w")
        file_result.write(frama_output)
        file_result.close()
        return getHtml(request)
    return render(request, 'app_name/add_file.html', context)  


@login_required
def del_file(request):
    existing_files = File.objects.all().filter(flag_available=True)
    context = {'files' : existing_files.filter(owner=request.user)}
    if request.method == 'POST' and request.POST.get('file'):
        f=File.objects.get(name=request.POST.get('file'))
        f.flag_available=False
        f.save()
        return getHtml(request)
    return render(request, 'app_name/del_file.html', context)  


@login_required
def add_dir(request):
    existing_dirs = Directory.objects.all().filter(flag_available=True)
    context = {'dirs' : existing_dirs.filter(owner=request.user)}
    if request.method == 'POST' and request.POST.get('fname') and \
        request.POST.get('fpdir') and \
        request.POST.get('fdesc'):
        d=Directory()
        d.name= request.POST.get('fname')
        d.owner= request.user
        if request.POST.get('fpdir') != "/":
            d.parent_directory= Directory.objects.get(name=request.POST.get('fpdir'))
        d.description = request.POST.get('fdesc')
        d.save()
        return getHtml(request)
    return render(request, 'app_name/add_dir.html', context)


@login_required
def del_dir(request):
    existing_dirs = Directory.objects.all().filter(flag_available=True)
    context = {'dirs' : existing_dirs.filter(owner=request.user)}
    if request.method == 'POST' and request.POST.get('fdir'):
        d=Directory.objects.get(name=request.POST.get('fdir'))
        d.flag_available = False
        d.save()
        return getHtml(request)
    return render(request, 'app_name/del_dir.html', context)


@login_required
def display_file(request, file_pk):
    settings.CURR_DISPLAYED = file_pk
    existing_files = File.objects.all().filter(flag_available=True)
    existing_dirs = Directory.objects.all().filter(flag_available=True)
    context = {}
    context['dirs'] = existing_dirs.filter(owner=request.user)
    context['files'] = existing_files.filter(owner=request.user)
    return render(request,'app_name/display_file.html', context)


# build html code for collapsible frama output sections
# according to rules for 'collapsible content' in main.css
def build_collapsible(sections):
    result = '<fieldset class="accordion-group-container">'
    i = 1
    for fs in sections:
        color = fs.status_color
        dsc = "<br />".join(fs.description.split("\n"))
        r = '<div class=\"accordion_container\"><input type=\"checkbox\" name=\"accordiongroup\" id=\"accordiongroup-'
        r += str(i)
        r += '\" class=\"accordion-control\"><label for=\"accordiongroup-'
        r += str(i)
        r += '\" class=\"accordion-label\" style=\"color:' + color + ';\">' + fs.section_category + ': ' + fs.status + '</label>'
        r += '<div class=\"accordion-content content-' + str(i) + '\" style=\"color:'+ color+';\">'
        r += dsc + '<br></div></div>'
        result += r
        i += 1
    result += '</fieldset>'
    return result


def choose_file(request):
    settings.CURR_DISPLAYED = request.GET['file']
    file = File.objects.get(pk=request.GET['file'])
    filename = file.name
    file_sections = FileSection.objects.all().filter(filename=filename)

    path = join(settings.MEDIA_ROOT, 'app_name', 'user_data', filename)
    fp = open(path, "r")
    file_content = fp.read()
    fp.close()

    path_result = result_path(path)
    frp = open(path_result, "r")
    file_frama_result = "<br />".join(frp.read().split("\n"))
    frp.close()
    file_frama_big_result = build_collapsible(file_sections)

    # send response
    file = model_to_dict(file)
    file_data = [file['id'], file['name'], file_content, file_frama_big_result]
    return JsonResponse({"file": list(file_data)}, status=200)
      

# for initializing home page with collapsible frama output
def get_collapsible(request):
    if request.method == 'GET' and settings.CURR_DISPLAYED != -1:
        file = File.objects.get(pk=settings.CURR_DISPLAYED)
        file_sections = FileSection.objects.all().filter(filename=file.name)
        file_frama_big_result = build_collapsible(file_sections)
        
        path = join(settings.MEDIA_ROOT, 'app_name', 'user_data', file.name)
        fp = open(path, "r")
        file_content = fp.read()
        fp.close()

        file_data = [file_frama_big_result, file_content]
        return JsonResponse({"file": list(file_data)}, status=200)
    return JsonResponse({"file": ""}, status=200)

def get_mode(request):
    return JsonResponse({"output": list([settings.MODE])}, status=200)
    
def set_mode(request):
    if request.method == 'GET':
        print("hi")
        settings.MODE = request.GET['mode']
        return JsonResponse(status=200)
    return JsonResponse(status=200)

def choose_prover(request):
    if request.method == 'POST' and settings.CURR_DISPLAYED != -1:
        file = File.objects.get(pk=settings.CURR_DISPLAYED)
        file.chosen_prover = request.POST.get('prover')
        file.save()
        # send response
        return JsonResponse({"prover": file.chosen_prover}, status=200)
    return JsonResponse({"prover": "0"}, status=200)


def choose_guard(request):
    if request.method == 'POST' and settings.CURR_DISPLAYED != -1:
        file = File.objects.get(pk=settings.CURR_DISPLAYED)
        file.chosen_flags = request.POST.get('vcs')
        file.save()
        # send response
        return JsonResponse({"guard": file.chosen_flags}, status=200)
    return JsonResponse({"guard": "0"}, status=200)


def run_prover(request):
    if request.method == 'GET' and request.is_ajax() and settings.CURR_DISPLAYED != -1:
        file = File.objects.get(pk=settings.CURR_DISPLAYED)
        path = join(settings.MEDIA_ROOT, 'app_name', 'user_data', file.name)
        if file.chosen_flags == "":
            command = 'frama-c -wp -wp-prover ' + file.chosen_prover + ' -wp-rte ' + path
        else:
            command = 'frama-c -wp -wp-prover ' + file.chosen_prover + ' -wp-prop="-@invariant" ' + path
        prover_output = "<br />".join(os.popen(command).read().split('\n'))
        now = datetime.now() + timedelta(hours=2)
        prover_output = now.strftime("%H:%M:%S") + '<br />' + prover_output
        
        # rerun frama on edited codemirror using file tmp.c
        new_code = request.GET['code']
        path_tmp = join(settings.MEDIA_ROOT, 'app_name', 'user_data', 'tmp.c')
        tmp = open(path_tmp, "w")
        tmp.write(new_code)
        tmp.close()

        FileSection.objects.filter(filename=file.name).delete()
        command = 'frama-c -wp -wp-print ' + path_tmp
        new_frama_output = os.popen(command).read()
        parse_result(new_frama_output.split('\n'), file.name)
        
        sections = FileSection.objects.filter(filename=file.name)
        pretty_new_frama = build_collapsible(sections)

        data = [prover_output, pretty_new_frama]
        # send response
        return JsonResponse({"output": list(data)}, status=200)
    return JsonResponse({"output": list([""])}, status=200)
