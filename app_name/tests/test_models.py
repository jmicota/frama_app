import datetime
from django.test import TestCase
from app_name.models import Directory, File, FileSection, StatusData
from django.contrib.auth.models import User
from unittest import mock


class DirectoryModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Directory.objects.create(name='dirname', description='dsc')

    def setUp(self):
        pass

    def test_name(self):
        d = Directory.objects.get(id=1)
        field_label = d.__str__()
        self.assertEqual(field_label, 'dirname')

    def test_name_length(self):
        d = Directory.objects.get(id=1)
        max_length = d._meta.get_field('name').max_length
        self.assertEqual(max_length, 50)

    def test_dsc(self):
        d = Directory.objects.get(id=1)
        field_label = d.description
        self.assertEqual(field_label, 'dsc')

    def test_dsc_length(self):
        d = Directory.objects.get(id=1)
        max_length = d._meta.get_field('description').max_length
        self.assertEqual(max_length, 100)

    def test_availability(self):
        d = Directory.objects.get(id=1)
        self.assertTrue(d.flag_available)


class FileModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Directory.objects.create(name='dirname')
        d = Directory.objects.get(id=1)
        File.objects.create(name='filename', parent_directory=d, description='dsc')

    def setUp(self):
        pass

    def test_name(self):
        f = File.objects.get(id=1)
        field_label = f.__str__()
        self.assertEqual(field_label, 'filename')

    def test_name_length(self):
        f = File.objects.get(id=1)
        max_length = f._meta.get_field('name').max_length
        self.assertEqual(max_length, 50)

    def test_dsc(self):
        f = File.objects.get(id=1)
        field_label = f.description
        self.assertEqual(field_label, 'dsc')

    def test_dsc_length(self):
        f = File.objects.get(id=1)
        max_length = f._meta.get_field('description').max_length
        self.assertEqual(max_length, 100)

    def test_availability(self):
        f = File.objects.get(id=1)
        self.assertTrue(f.flag_available)

    def test_flags(self):
        f = File.objects.get(id=1)
        max_length = f._meta.get_field('chosen_flags').max_length
        self.assertEqual(f.chosen_flags, ' ')
        self.assertEqual(max_length, 50)
    
    def test_prover(self):
        f = File.objects.get(id=1)
        max_length = f._meta.get_field('chosen_prover').max_length
        self.assertEqual(f.chosen_prover, 'alt-ergo')
        self.assertEqual(max_length, 10)

    def test_parent_directory(self):
        f = File.objects.get(id=1)
        d = f.parent_directory.name
        self.assertEqual(d, 'dirname')


class StatusDataModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        StatusData.objects.create(data='my data is great')

    def setUp(self):
        pass

    def test_data(self):
        sd = StatusData.objects.get(id=1)
        field_label = sd.__str__()
        max_length = sd._meta.get_field('data').max_length
        self.assertEqual(field_label, 'my data is great')
        self.assertEqual(max_length, 999)

    def test_valid(self):
        sd = StatusData.objects.get(id=1)
        self.assertTrue(sd.flag_valid)


class FileSectionModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        StatusData.objects.create(data='my data is great')
        sd = StatusData.objects.get(id=1)
        FileSection.objects.create(description="dsc", status_data=sd)

    def setUp(self):
        pass

    def test_to_str(self):
        fs = FileSection.objects.get(id=1)
        field_label = fs.__str__()
        self.assertEqual(field_label, 'dsc')

    def test_section_category(self):
        fs = FileSection.objects.get(id=1)
        field_label = fs.section_category
        max_length = fs._meta.get_field('section_category').max_length
        self.assertEqual(field_label, 'Invariant')
        self.assertEqual(max_length, 10)

    def test_filename(self):
        fs = FileSection.objects.get(id=1)
        max_length = fs._meta.get_field('filename').max_length
        self.assertEqual(max_length, 50)

    def test_dsc(self):
        fs = FileSection.objects.get(id=1)
        field_label = fs.description
        max_length = fs._meta.get_field('description').max_length
        self.assertEqual(field_label, 'dsc')
        self.assertEqual(max_length, 999)

    def test_status(self):
        fs = FileSection.objects.get(id=1)
        field_label = fs.status
        max_length = fs._meta.get_field('status').max_length
        self.assertEqual(field_label, 'Unchecked')
        self.assertEqual(max_length, 10)

    def test_status_color(self):
        fs = FileSection.objects.get(id=1)
        field_label = fs.status_color
        max_length = fs._meta.get_field('status_color').max_length
        self.assertEqual(field_label, 'black')
        self.assertEqual(max_length, 20)

    def test_validity(self):
        fs = FileSection.objects.get(id=1)
        self.assertTrue(fs.flag_valid)

    def test_status_data(self):
        fs = FileSection.objects.get(id=1)
        d = fs.status_data.data
        self.assertEqual(d, 'my data is great')
