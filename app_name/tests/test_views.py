from django.test import TestCase, Client
from django.urls import reverse
from django.http import JsonResponse, HttpResponseBadRequest
from http import HTTPStatus
from django.views.generic import View
from app_name.models import Directory, File, FileSection, StatusData
from django.contrib.auth.models import User
import json
from app_name import views
from django.conf import settings
from os.path import join


class ParsingFilesTest(TestCase):

    def setUp(self):
        self.file1 = File.objects.create(name='file1.c', description='dsc')
        self.path = 'this/is/path/to/file.c'
        self.line = 'Prover Qed returns Valid (4ms)'
        self.to_parse = ['Goal Post-condition (file app_name/user_data/file1.c, line 5) in \'abs\':',
                         'Prove: true.',
                         'Prover Qed returns Valid (3ms)',
                         'Goal Post-condition (file app_name/user_data/file1.c, line 10) in \'abs\':',
                         'Prove: false.',
                         'Prover Qed returns Invalid (5ms)']

    def test_result_path(self):
        expected_result = 'this/is/path/to/file-result.c'
        self.assertEquals(expected_result, views.result_path(self.path))

    def test_status_code(self):
        self.assertEquals(views.status(self.line), 'Valid')

    def test_status_color(self):
        self.assertEquals(views.status_color(views.status(self.line)), 'green')
        self.assertEquals(views.status_color('Invalid'), 'Crimson')
        self.assertEquals(views.status_color('Unknown'), 'BlueViolet')
        self.assertEquals(views.status_color('Whatever'), 'DimGrey')

    def test_section_name(self):
        self.assertEquals(views.section_name(self.to_parse[0]), 'Post-condition')
        self.assertEquals(views.section_name('Goal Pre-condition (file app_name/user_data/file1.c, line 5) in \'abs\':'), 'Pre-condition')
        self.assertEquals(views.section_name('Goal Assertion (file app_name/user_data/file1.c, line 5) in \'abs\':'), 'Assertion')
        self.assertEquals(views.section_name('Goal Preservation of Invariant (file app_name/user_data/file1.c, line 5) in \'abs\':'), 'Invariant')
        self.assertEquals(views.section_name('Goal Establishment of Invariant (file app_name/user_data/file1.c, line 5) in \'abs\':'), 'Invariant')

    def test_parse_result(self):
        self.assertEqual(FileSection.objects.all().count(), 0)
        
        views.parse_result(self.to_parse, 'file1.c')

        self.assertEqual(FileSection.objects.all().count(), 2)

        fs1 = FileSection.objects.get(pk=1)
        fs1_expected = self.to_parse[0] + '\n' + self.to_parse[1] + '\n' + self.to_parse[2] + '\n'
        fs2 = FileSection.objects.get(pk=2)
        fs2_expected = self.to_parse[3] + '\n' + self.to_parse[4] + '\n' + self.to_parse[5] + '\n'
        
        self.assertEqual(fs1.filename, fs2.filename)
        self.assertEqual(fs1.filename, 'file1.c')
        self.assertEqual(fs1_expected, fs1.description)
        self.assertEqual(fs2_expected, fs2.description)
        self.assertEqual(fs1.section_category, 'Post-condition')
        self.assertEqual(fs2.section_category, 'Post-condition')
        self.assertEqual(fs1.status, 'Valid')
        self.assertEqual(fs2.status, 'Invalid')

    def test_build_collapsible(self):
        views.parse_result(self.to_parse, 'file1.c')
        expected = '<fieldset class="accordion-group-container"><div class="accordion_container"><input type="checkbox" name="accordiongroup" id="accordiongroup-1" class="accordion-control"><label for="accordiongroup-1" class="accordion-label" style="color:green;">Post-condition: Valid</label><div class="accordion-content content-1" style="color:green;">Goal Post-condition (file app_name/user_data/file1.c, line 5) in \'abs\':<br />Prove: true.<br />Prover Qed returns Valid (3ms)<br /><br></div></div><div class="accordion_container"><input type="checkbox" name="accordiongroup" id="accordiongroup-2" class="accordion-control"><label for="accordiongroup-2" class="accordion-label" style="color:Crimson;">Post-condition: Invalid</label><div class="accordion-content content-2" style="color:Crimson;">Goal Post-condition (file app_name/user_data/file1.c, line 10) in \'abs\':<br />Prove: false.<br />Prover Qed returns Invalid (5ms)<br /><br></div></div></fieldset>'
        self.assertEqual(expected, views.build_collapsible(FileSection.objects.all()))


class BottomTabChoicesTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.file = File.objects.create(name='file1.c')

    def test_display_file(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(settings.CURR_DISPLAYED, 1)

    def test_choose_file(self):
        settings.CURR_DISPLAYED = 1
        fpk = File.objects.get(name='file1.c').pk
        response = self.client.get('/choose_file/', {'file': fpk})
        expected = [fpk, 'file1.c']
        self.assertEquals(response.status_code, 200)
        self.assertEqual(response.json()['file'][0], expected[0])
        self.assertEqual(response.json()['file'][1], expected[1])

    def test_choose_prover_correct_req(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.post('/choose_prover/', {'prover': 'cvc4'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'prover': 'cvc4',
        })

    def test_choose_prover_incorrect_req(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.get('/choose_prover/', {'prover': 'cvc4'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'prover': '0',
        })

    def test_choose_guard_correct_req(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.post('/choose_guard/', {'vcs': '-@invariant'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'guard': '-@invariant',
        })

    def test_choose_guard_incorrect_req(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.get('/choose_guard/', {'vcs': '-@invariant'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'guard': '0',
        })

    def test_run_prover(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.get('/run_prover/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'output': [''],
        })

    def test_get_collapsible_correct_req(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.get('/get_collapsible/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['file'][0], '<fieldset class="accordion-group-container"></fieldset>')
        self.assertEqual(response.json()['file'][1], '')

    def test_get_collapsible_incorrect_req(self):
        settings.CURR_DISPLAYED = 1
        response = self.client.post('/get_collapsible/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'file': '',
        })


class RedirectingLinksTest(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="user", password="password")
        d = Directory.objects.create(name='dirname')
        self.file1 = File.objects.create(name='file1.c', owner=self.user, parent_directory=d, description='dsc')
        self.del_file_url = reverse('del_file')
        self.add_file_url = reverse('add_file')
        self.del_dir_url = reverse('del_dir')
        self.add_dir_url = reverse('add_dir')
        self.register_url = reverse('register')

    def test_register_load(self):
        response = self.client.get(self.register_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'app_name/register.html')

    def test_add_file_load_not_logged_in(self):
        response = self.client.get(self.add_file_url)
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
    
    def test_add_dir_load_not_logged_in(self):
        response = self.client.get(self.add_dir_url)
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
    
    def test_del_file_load_not_logged_in(self):
        response = self.client.get(self.del_file_url)
        self.assertEquals(response.status_code, HTTPStatus.FOUND)

    def test_del_dir_load_not_logged_in(self):
        response = self.client.get(self.del_dir_url)
        self.assertEquals(response.status_code, HTTPStatus.FOUND)

    def test_add_file_load(self):
        self.client.force_login(self.user)
        response = self.client.get(self.add_file_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'app_name/add_file.html')

    def test_add_dir_load(self):
        self.client.force_login(self.user)
        response = self.client.get(self.add_dir_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'app_name/add_dir.html')

    def test_del_file_load(self):
        self.client.force_login(self.user)
        response = self.client.get(self.del_file_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'app_name/del_file.html')

    def test_del_dir_load(self):
        self.client.force_login(self.user)
        response = self.client.get(self.del_dir_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'app_name/del_dir.html')

    def test_getHtml(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)

    def test_getHtml(self):
        self.client.force_login(self.user)
        response = self.client.get('/')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'app_name/display_file.html')

    def test_add_file(self):
        self.client.force_login(self.user)
        response = self.client.post(self.add_file_url, {
            'fname': 'user_file.c',
            'fpdir': '/',
            'fcont': 'file contents here',
            'fdesc': 'dsc'
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertEquals(File.objects.count(), 2)
        self.assertTemplateUsed(response, 'app_name/display_file.html')

    def test_add_dir(self):
        self.client.force_login(self.user)
        response = self.client.post(self.add_dir_url, {
            'fname': 'dir1',
            'fpdir': '/',
            'fdesc': 'dsc'
        })
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEquals(Directory.objects.count(), 2)
        self.assertTemplateUsed(response, 'app_name/display_file.html')

    def test_delete_dir(self):
        self.client.force_login(self.user)
        response = self.client.post(self.del_dir_url, {
            'fdir': 'dirname'
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertEquals(Directory.objects.count(), 1)
        self.assertTemplateUsed(response, 'app_name/display_file.html')

    def test_delete_file(self):
        self.client.force_login(self.user)
        response = self.client.post(self.del_file_url, {
            'file': 'file1.c'
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertEquals(File.objects.count(), 1)
        self.assertTemplateUsed(response, 'app_name/display_file.html')