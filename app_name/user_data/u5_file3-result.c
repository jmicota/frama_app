[kernel] Parsing app_name/user_data/u5_file3.c (with preprocessing)
[wp] Running WP plugin...
[wp] warning: Missing RTE guards
[wp] 2 goals scheduled
[wp] [Qed] Goal typed_abs_post_2 : Valid
[wp] [Qed] Goal typed_abs_post : Valid
[wp] Proved goals:    2 / 2
    Qed:             2  (3ms-4ms)
------------------------------------------------------------
  Function abs
------------------------------------------------------------

Goal Post-condition (file app_name/user_data/u5_file3.c, line 3) in 'abs':
Prove: true.
Prover Qed returns Valid (4ms)

------------------------------------------------------------

Goal Post-condition (file app_name/user_data/u5_file3.c, line 5) in 'abs':
Prove: true.
Prover Qed returns Valid (3ms)

------------------------------------------------------------
