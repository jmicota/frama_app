[kernel] Parsing app_name/user_data/u5_file1.c (with preprocessing)
app_name/user_data/u5_file1.c:7:[kernel] warning: parsing obsolete ACSL construct '\valid_range(addr,min,max)'. '\valid(addr+(min..max))' should be used instead.
[wp] Running WP plugin...
[wp] warning: Missing RTE guards
app_name/user_data/u5_file1.c:28:[wp] warning: Missing assigns clause (assigns 'everything' instead)
app_name/user_data/u5_file1.c:18:[wp] warning: Missing assigns clause (assigns 'everything' instead)
[wp] 17 goals scheduled
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_established : Valid
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_preserved : Unknown (Qed:17ms) (55ms)
[wp] [Alt-Ergo] Goal typed_insert_sort_post : Unknown (Qed:6ms) (104ms)
[wp] [Qed] Goal typed_insert_sort_loop_inv_3_established : Valid
[wp] [Qed] Goal typed_insert_sort_loop_inv_4_preserved : Valid
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_2_preserved : Valid
[wp] [Qed] Goal typed_insert_sort_loop_inv_4_established : Valid
[wp] [Qed] Goal typed_insert_sort_loop_inv_5_established : Valid
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_3_preserved : Valid
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_2_established : Valid
[wp] [Qed] Goal typed_insert_sort_loop_inv_6_established : Valid
[wp] [Qed] Goal typed_insert_sort_loop_term_positive : Valid
[wp] [Qed] Goal typed_insert_sort_loop_term_2_decrease : Valid
[wp] [Qed] Goal typed_insert_sort_loop_term_2_positive : Valid
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_5_preserved : Valid
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_term_decrease : Unknown (Qed:18ms) (61ms)
[wp] [Alt-Ergo] Goal typed_insert_sort_loop_inv_6_preserved : Valid
[wp] Proved goals:   14 / 17
    Qed:             8  (0.45ms-8ms-24ms)
    Alt-Ergo:        6  (4ms-45ms-127ms) (365) (unknown: 3)
------------------------------------------------------------
  Function insert_sort
------------------------------------------------------------

Goal Post-condition (file app_name/user_data/u5_file1.c, line 9) in 'insert_sort':
Assume {
  Type: is_sint32(i) /\ is_sint32(n) /\ is_sint32(n_1).
  (* Heap *)
  Have: (region(t.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t, 0), n).
  If n <= 1
  Else {
    (* Invariant *)
    Have: P_Sorted(Mint_0, t_1, 0, i).
    (* Invariant *)
    Have: (0 <= i) /\ (i <= n_1).
    (* Else *)
    Have: n_1 <= i.
  }
}
Prove: P_Sorted(Mint_0, t, 0, n - 1).
Prover Alt-Ergo returns Unknown (Qed:6ms) (104ms)

------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/u5_file1.c, line 14):
Let x = 1 + i.
Assume {
  Type: is_sint32(i_1) /\ is_sint32(i) /\ is_sint32(j) /\ is_sint32(mv_0) /\
      is_sint32(n_1) /\ is_sint32(n) /\ is_sint32(n_2) /\ is_sint32(x).
  (* Heap *)
  Have: (region(t.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t, 0), n_1).
  (* Else *)
  Have: 2 <= n_1.
  (* Invariant *)
  Have: P_Sorted(Mint_0, t_1, 0, i_1).
  (* Invariant *)
  Have: (0 <= i_1) /\ (i_1 <= n_2).
  (* Then *)
  Have: i_1 < n_2.
  (* Invariant *)
  Have: forall i_2 : Z. ((i_2 < i) -> ((j <= i_2) ->
      (mv_1 < Mint_1[shift_sint32(t_2, i_2)]))).
  (* Invariant *)
  Have: ((j < i) -> P_Sorted(Mint_1, t_2, 0, x)).
  (* Invariant *)
  Have: ((j = i) -> P_Sorted(Mint_1, t_2, 0, i)).
  (* Invariant *)
  Have: (j <= i) /\ (0 <= j).
  If 0 < j
  Then {
    Have: (Mint_1 = Mint_2) /\ (mv_1 = mv_0) /\ (t_2 = t_3).
    (* Then *)
    Have: Mint_2[shift_sint32(t_3, j - 1)] <= mv_0.
  }
}
Prove: (i < n) /\ ((-1) <= i).
Prover Alt-Ergo returns Unknown (Qed:17ms) (55ms)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/u5_file1.c, line 14):
Assume {
  Type: is_sint32(n).
  (* Heap *)
  Have: (region(t.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t, 0), n).
  (* Else *)
  Have: 2 <= n.
}
Prove: 0 < n.
Prover Alt-Ergo returns Valid (Qed:2ms) (4ms) (10)

------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/u5_file1.c, line 16):
Let x = 1 + i.
Assume {
  Type: is_sint32(i_1) /\ is_sint32(i) /\ is_sint32(j) /\ is_sint32(n) /\
      is_sint32(n_1) /\ is_sint32(x).
  (* Heap *)
  Have: (region(t_1.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t_1, 0), n).
  (* Else *)
  Have: 2 <= n.
  (* Invariant *)
  Have: P_Sorted(Mint_1, t_2, 0, i_1).
  (* Invariant *)
  Have: (0 <= i_1) /\ (i_1 <= n_1).
  (* Then *)
  Have: i_1 < n_1.
  (* Invariant *)
  Have: forall i_2 : Z. ((i_2 < i) -> ((j <= i_2) ->
      (mv_0 < Mint_0[shift_sint32(t, i_2)]))).
  (* Invariant *)
  Have: ((j < i) -> P_Sorted(Mint_0, t, 0, x)).
  (* Invariant *)
  Have: ((j = i) -> P_Sorted(Mint_0, t, 0, i)).
  (* Invariant *)
  Have: (j <= i) /\ (0 <= j).
  If 0 < j
  Then { (* Then *) Have: Mint_0[shift_sint32(t, j - 1)] <= mv_0. }
}
Prove: P_Sorted(Mint_0[shift_sint32(t, j) <- mv_0], t, 0, x).
Prover Alt-Ergo returns Valid (Qed:23ms) (127ms) (355)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/u5_file1.c, line 16):
Assume {
  Type: is_sint32(n).
  (* Heap *)
  Have: (region(t.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t, 0), n).
  (* Else *)
  Have: 2 <= n.
}
Prove: P_Sorted(Mint_0, t, 0, 1).
Prover Alt-Ergo returns Valid (Qed:8ms) (4ms) (11)

------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/u5_file1.c, line 21):
Let x = j - 1.
Let x_1 = Mint_0[shift_sint32(t, x)].
Let x_2 = 1 + i.
Assume {
  Type: is_sint32(i_1) /\ is_sint32(i) /\ is_sint32(j) /\ is_sint32(mv_0) /\
      is_sint32(n) /\ is_sint32(n_1) /\ is_sint32(x) /\ is_sint32(x_1).
  (* Heap *)
  Have: (region(t_1.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t_1, 0), n).
  (* Else *)
  Have: 2 <= n.
  (* Invariant *)
  Have: P_Sorted(Mint_1, t_2, 0, i_1).
  (* Invariant *)
  Have: (0 <= i_1) /\ (i_1 <= n_1).
  (* Then *)
  Have: i_1 < n_1.
  (* Invariant *)
  Have: forall i_2 : Z. ((i_2 < i) -> ((j <= i_2) ->
      (mv_0 < Mint_0[shift_sint32(t, i_2)]))).
  (* Invariant *)
  Have: ((j < i) -> P_Sorted(Mint_0, t, 0, x_2)).
  (* Invariant *)
  Have: ((j = i) -> P_Sorted(Mint_0, t, 0, i)).
  (* Invariant *)
  Have: (j <= i) /\ (0 <= j).
  (* Then *)
  Have: 0 < j.
  (* Else *)
  Have: mv_0 < x_1.
}
Prove: j <= x_2.
Prover Alt-Ergo returns Valid (Qed:15ms) (11ms) (28)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/u5_file1.c, line 21):
Prove: true.
Prover Qed returns Valid (2ms)

------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/u5_file1.c, line 23):
Prove: true.
Prover Qed returns Valid (4ms)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/u5_file1.c, line 23):
Prove: true.
Prover Qed returns Valid (3ms)

------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/u5_file1.c, line 25):
Let x = j - 1.
Let x_1 = Mint_0[shift_sint32(t, x)].
Let x_2 = 1 + i.
Assume {
  Type: is_sint32(i_1) /\ is_sint32(i) /\ is_sint32(j) /\ is_sint32(mv_0) /\
      is_sint32(n) /\ is_sint32(n_1) /\ is_sint32(x) /\ is_sint32(x_1).
  (* Goal *)
  When: j <= i.
  (* Heap *)
  Have: (region(t_1.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t_1, 0), n).
  (* Else *)
  Have: 2 <= n.
  (* Invariant *)
  Have: P_Sorted(Mint_1, t_2, 0, i_1).
  (* Invariant *)
  Have: (0 <= i_1) /\ (i_1 <= n_1).
  (* Then *)
  Have: i_1 < n_1.
  (* Invariant *)
  Have: forall i_2 : Z. ((i_2 < i) -> ((j <= i_2) ->
      (mv_0 < Mint_0[shift_sint32(t, i_2)]))).
  (* Invariant *)
  Have: ((j < i) -> P_Sorted(Mint_0, t, 0, x_2)).
  (* Invariant *)
  Have: ((j = i) -> P_Sorted(Mint_0, t, 0, i)).
  (* Invariant *)
  Have: 0 <= j.
  (* Then *)
  Have: 0 < j.
  (* Else *)
  Have: mv_0 < x_1.
}
Prove: P_Sorted(Mint_0[shift_sint32(t, j) <- x_1], t, 0, x_2).
Prover Alt-Ergo returns Valid (Qed:19ms) (107ms) (365)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/u5_file1.c, line 25):
Prove: true.
Prover Qed returns Valid (2ms)

------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/u5_file1.c, line 27):
Let x = j - 1.
Let x_1 = Mint_0[shift_sint32(t, x)].
Assume {
  Type: is_sint32(i_1) /\ is_sint32(i_2) /\ is_sint32(j) /\
      is_sint32(mv_0) /\ is_sint32(n) /\ is_sint32(n_1) /\ is_sint32(x) /\
      is_sint32(x_1).
  (* Goal *)
  When: (i < i_2) /\ (j <= (1 + i)).
  (* Heap *)
  Have: (region(t_1.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t_1, 0), n).
  (* Else *)
  Have: 2 <= n.
  (* Invariant *)
  Have: P_Sorted(Mint_1, t_2, 0, i_1).
  (* Invariant *)
  Have: (0 <= i_1) /\ (i_1 <= n_1).
  (* Then *)
  Have: i_1 < n_1.
  (* Invariant *)
  Have: forall i_3 : Z. ((i_3 < i_2) -> ((j <= i_3) ->
      (mv_0 < Mint_0[shift_sint32(t, i_3)]))).
  (* Invariant *)
  Have: ((j < i_2) -> P_Sorted(Mint_0, t, 0, 1 + i_2)).
  (* Invariant *)
  Have: ((j = i_2) -> P_Sorted(Mint_0, t, 0, i_2)).
  (* Invariant *)
  Have: (j <= i_2) /\ (0 <= j).
  (* Then *)
  Have: 0 < j.
  (* Else *)
  Have: mv_0 < x_1.
}
Prove: mv_0 < Mint_0[shift_sint32(t, j) <- x_1][shift_sint32(t, i)].
Prover Alt-Ergo returns Valid (Qed:24ms) (14ms) (40)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/u5_file1.c, line 27):
Prove: true.
Prover Qed returns Valid

------------------------------------------------------------

Goal Decreasing of Loop variant at loop (file app_name/user_data/u5_file1.c, line 18):
Let x = 1 + i_1.
Assume {
  Type: is_sint32(i) /\ is_sint32(i_1) /\ is_sint32(j) /\ is_sint32(mv_0) /\
      is_sint32(n_2) /\ is_sint32(n) /\ is_sint32(n_1) /\ is_sint32(x).
  (* Heap *)
  Have: (region(t.base) <= 0) /\ linked(Malloc_0).
  (* Pre-condition *)
  Have: valid_rw(Malloc_0, shift_sint32(t, 0), n_2).
  (* Else *)
  Have: 2 <= n_2.
  (* Invariant *)
  Have: P_Sorted(Mint_0, t_1, 0, i).
  (* Invariant *)
  Have: (0 <= i) /\ (i <= n_1).
  (* Then *)
  Have: i < n_1.
  (* Invariant *)
  Have: forall i_2 : Z. ((i_2 < i_1) -> ((j <= i_2) ->
      (mv_1 < Mint_1[shift_sint32(t_2, i_2)]))).
  (* Invariant *)
  Have: ((j < i_1) -> P_Sorted(Mint_1, t_2, 0, x)).
  (* Invariant *)
  Have: ((j = i_1) -> P_Sorted(Mint_1, t_2, 0, i_1)).
  (* Invariant *)
  Have: (j <= i_1) /\ (0 <= j).
  If 0 < j
  Then {
    Have: (Mint_1 = Mint_2) /\ (mv_1 = mv_0) /\ (t_2 = t_3).
    (* Then *)
    Have: Mint_2[shift_sint32(t_3, j - 1)] <= mv_0.
  }
}
Prove: (i + n) <= (i_1 + n_1).
Prover Alt-Ergo returns Unknown (Qed:18ms) (61ms)

------------------------------------------------------------

Goal Positivity of Loop variant at loop (file app_name/user_data/u5_file1.c, line 18):
Prove: true.
Prover Qed returns Valid (3ms)

------------------------------------------------------------

Goal Decreasing of Loop variant at loop (file app_name/user_data/u5_file1.c, line 28):
Prove: true.
Prover Qed returns Valid (5ms)

------------------------------------------------------------

Goal Positivity of Loop variant at loop (file app_name/user_data/u5_file1.c, line 28):
Prove: true.
Prover Qed returns Valid (5ms)

------------------------------------------------------------
