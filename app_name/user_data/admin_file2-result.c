[kernel] Parsing app_name/user_data/admin_file2.c (with preprocessing)
[wp] Running WP plugin...
[wp] warning: Missing RTE guards
app_name/user_data/admin_file2.c:7:[wp] warning: Missing assigns clause (assigns 'everything' instead)
[wp] 3 goals scheduled
[wp] [Qed] Goal typed_main_assert : Valid
[wp] [Qed] Goal typed_main_loop_inv_established : Valid
[wp] [Alt-Ergo] Goal typed_main_loop_inv_preserved : Valid
[wp] Proved goals:    3 / 3
    Qed:             2  (0.08ms-0.97ms-3ms)
    Alt-Ergo:        1  (4ms) (8)
------------------------------------------------------------
  Function main
------------------------------------------------------------

Goal Preservation of Invariant (file app_name/user_data/admin_file2.c, line 6):
Assume {
  Type: is_sint32(i) /\ is_sint32(1 + i).
  (* Invariant *)
  Have: (0 <= i) /\ (i <= 30).
  (* Then *)
  Have: i <= 29.
}
Prove: (-1) <= i.
Prover Alt-Ergo returns Valid (Qed:3ms) (4ms) (8)

------------------------------------------------------------

Goal Establishment of Invariant (file app_name/user_data/admin_file2.c, line 6):
Prove: true.
Prover Qed returns Valid

------------------------------------------------------------

Goal Assertion (file app_name/user_data/admin_file2.c, line 10):
Prove: true.
Prover Qed returns Valid

------------------------------------------------------------
