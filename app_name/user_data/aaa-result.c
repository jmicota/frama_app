[kernel] Parsing app_name/user_data/aaa.c (with preprocessing)
[wp] Running WP plugin...
[wp] warning: Missing RTE guards
[wp] 6 goals scheduled
[wp] [Qed] Goal typed_abs2_post_2 : Valid
[wp] [Qed] Goal typed_abs2_post : Valid
[wp] [Qed] Goal typed_abs_post_2 : Valid
[wp] [Qed] Goal typed_abs_post : Valid
[wp] [Qed] Goal typed_abs3_post_2 : Valid
[wp] [Qed] Goal typed_abs3_post : Valid
[wp] Proved goals:    6 / 6
    Qed:             6  (3ms-5ms-9ms)
------------------------------------------------------------
  Function abs
------------------------------------------------------------

Goal Post-condition (file app_name/user_data/aaa.c, line 3) in 'abs':
Prove: true.
Prover Qed returns Valid (4ms)

------------------------------------------------------------

Goal Post-condition (file app_name/user_data/aaa.c, line 5) in 'abs':
Prove: true.
Prover Qed returns Valid (3ms)

------------------------------------------------------------
------------------------------------------------------------
  Function abs2
------------------------------------------------------------

Goal Post-condition (file app_name/user_data/aaa.c, line 13) in 'abs2':
Prove: true.
Prover Qed returns Valid (3ms)

------------------------------------------------------------

Goal Post-condition (file app_name/user_data/aaa.c, line 15) in 'abs2':
Prove: true.
Prover Qed returns Valid (3ms)

------------------------------------------------------------
------------------------------------------------------------
  Function abs3
------------------------------------------------------------

Goal Post-condition (file app_name/user_data/aaa.c, line 23) in 'abs3':
Prove: true.
Prover Qed returns Valid (9ms)

------------------------------------------------------------

Goal Post-condition (file app_name/user_data/aaa.c, line 25) in 'abs3':
Prove: true.
Prover Qed returns Valid (7ms)

------------------------------------------------------------
