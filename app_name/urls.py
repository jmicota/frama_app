from django.urls import path
from django.conf.urls import url
from app_name import views

urlpatterns = [
    url(r'^register/$', views.register, name='register'),
    path("", views.getHtml, name="html_page"),
    path("add_file/", views.add_file, name="add_file"),
    path("add_dir/", views.add_dir, name="add_dir"),
    path("del_dir/", views.del_dir, name="del_dir"),
    path("del_file/", views.del_file, name="del_file"),
    path("choose_file/", views.choose_file, name="choose_file"),
    path("choose_prover/", views.choose_prover, name="choose_prover"),
    path("choose_guard/", views.choose_guard, name="choose_guard"),
    path("run_prover/", views.run_prover, name="run_prover"),
    path("get_collapsible/", views.get_collapsible, name="get_collapsible"),
    path("get_mode/", views.get_mode, name="get_mode"),
    path("set_mode/", views.set_mode, name="set_mode"),
]
