from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import TemplateView
from app_name import views

urlpatterns = [
    path("", include("app_name.urls")),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r"^register/", views.register, name="register"),
]

urlpatterns += staticfiles_urlpatterns()